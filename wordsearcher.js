var assert = require('assert');
var fs = require('fs');

/*
Class to contain all functionality and variables needed for the search.
Properties:
    * h - height of the grid (number of rows)
    * w - width of the grid (number of columns)
    * gridRows - array of strings representing the rows
    * gridColumns - array of strings representing the columns
    * gridDiagonalForwards - array of strings representing the forward diagonal
    direction
    * gridDiagonalBackwards - array of strings representing the backwards diagonal
    direction
    * wordList - array of strings containing words to search for
    * matches - a dictionary whose keys are the words from the wordList, and
    whose values are documents containing information on the location of the
    matches
*/
function WordSearcher() {}

/*
Initializes the properties of the WordSearcher instance using the file provided
*/
WordSearcher.prototype.initialize = function () {
    assert(process.argv.length == 3, 'Missing path to word search grid.');

    var path = process.argv[2];

    var fileContents = fs.readFileSync(path, 'ascii')
        .split('\n')
        .filter(Boolean);

    this.h = parseInt(fileContents[0].split('x')[0]);
    this.w = parseInt(fileContents[0].split('x')[1]);

    this.gridRows = [];
    this.gridColumns = [];
    this.gridDiagonalForwards = [];
    this.gridDiagonalBackwards = [];

    this.wordList = [];

    this.matches = {};

    //Process word list
    for (var i = 1 + this.h; i < fileContents.length; i++) {
        var word = fileContents[i]
        if (word.length > 0) { this.wordList.push(word); }
    }

    fileContents = fileContents.map((line) => {
        return line.replace(/ /g,'')
    })

    //Process rows
    for (var y = 0; y < this.h; y++) {
        this.gridRows.push(fileContents[y+1]);
    }

    //Process columns
    for (var x = 0; x < this.w; x++) {
        var column = '';
        for (var y = 0; y < this.h; y++) {
            column += fileContents[y+1].charAt(x);
        }
        this.gridColumns.push(column);
    }

    //Process diagonal forwards
    for (var x = 0; x < this.w; x++) {
        var str = '';
        var y = 0;
        while (x - y > -1 && y < this.h) {
            str += fileContents[y+1].charAt(x - y);
            y++;
        }
        this.gridDiagonalForwards.push(str);
    }
    for (var y = 1; y < this.h; y++) {
        var str = '';
        var x = this.w - 1;
        while (x > -1 && this.w-x+y-1 < this.h) {
            str += fileContents[this.w-x+y].charAt(x);
            x--;
        }
        this.gridDiagonalForwards.push(str);
    }

    //Process diagonal backwards
    for (var y = this.h - 1; y > 0; y--) {
        var str = '';
        var x = 0;
        while (x < this.w && y+x < this.h) {
            str += fileContents[y+x+1].charAt(x);
            x++;
        }
        this.gridDiagonalBackwards.push(str);
    }
    for (var x = 0; x < this.w; x++) {
        var str = '';
        var y = 0;
        while (x + y < this.w && y < this.h) {
            str += fileContents[y+1].charAt(x + y);
            y++;
        }
        this.gridDiagonalBackwards.push(str);
    }
};

/*
Searches for matches in all possible direction while keeping track of
the remaining words to search for
*/
WordSearcher.prototype.beginSearch = function() {
    var remainingWords = this.wordList.slice();

    this.searchRows(remainingWords);
    this.searchColumns(remainingWords);
    this.searchDiagonal(remainingWords);
}

/*
Searches all rows for any remaining words. All matches are then processed and
inserted into the matches dictionary
*/
WordSearcher.prototype.searchRows = function (remainingWords) {
    var matches = this.search(remainingWords, this.gridRows);

    matches.forEach((detail) => {
        this.matches[detail.word] = detail.index + ':' + detail.min
                                    + ' ' +
                                    detail.index + ':' + detail.max;
    });
};

/*
Searches all columns for any remaining words. All matches are then processed and
inserted into the matches dictionary
*/
WordSearcher.prototype.searchColumns = function (remainingWords) {
    var matches = this.search(remainingWords, this.gridColumns)

    matches.forEach((detail) => {
        this.matches[detail.word] = detail.min + ':' + detail.index
                                    + ' ' +
                                    detail.max + ':' + detail.index;
    });
};

/*
Searches all digonal lines for any remaining words. All matches are then
processed and inserted into the matches dictionary
*/
WordSearcher.prototype.searchDiagonal = function (remainingWords) {
    var matches = this.search(remainingWords, this.gridDiagonalForwards);
    this.processMatchesDiagonalForwards(matches);

    matches = this.search(remainingWords, this.gridDiagonalBackwards);
    this.processMatchesDiagonalBackwards(matches);
};

/*
Main search function. Iterates through the remainingWords against each string
in arr and looks for matches. Searches for a word in a string by starting at
the middle and expanding outwards in both directions.
*/
WordSearcher.prototype.search = function (remainingWords, arr) {
    var matches = [];
    for (var v = 0; v < remainingWords.length; v++) {
        // Remove whitespace
        var word = remainingWords[v].replace(/ /g, '');
        var step = 1;
        var details;
        for (var x = 0; x < arr.length; x++) {

            // Index for left-side search
            var i = Math.floor(arr[x].length / 2) - step;
            // Index for right-side search
            var j = Math.floor(arr[x].length / 2) + step;

            // Check if the match is in the middle of the string
            details = this.findMatch(Math.floor(arr[x].length / 2), word, arr, x);

            // Search left side of string
            while (i > -1 && !details.match) {
                details = this.findMatch(i, word, arr, x);
                i -= step;
            }
            // Search right side of string
            while (j < arr[x].length && !details.match) {
                details = this.findMatch(j, word, arr, x);
                j += step;
            }

            // Break if a match has been found
            if (details.match) { x = arr.length; }
        }

        // Add match document to list of match details and remove word from
        // list of remaining words.
        if (details.match) {
            matches.push(details);
            remainingWords.splice(v, 1);
            v--;
        }
    }
    // List of match details
    return matches;
};

/*
Given a word, starting index, and a string, look for substring that matches the
word

Returns:
    match: boolean of match success,
    word: word being searched for (optional),
    index: index of string in the array being searched (optional),
    min: the minimum character index in the string being searched (optional),
    max: the minimum character index in the string being searched (optional)
*/
WordSearcher.prototype.findMatch = function (start, word, arr, index) {
    var str = arr[index];
    var char = str.charAt(start);
    var i = 0;

    // While the word contains the character we are currently looking at, grab
    // the substring of the entire range in which word could be.
    while (word.indexOf(char, i) != -1) {
        i = word.indexOf(char, i);
        //Start position of the word in the string
        min = start - i;
        //End position of the word in the string
        max = start + ((word.length - 1) - i);
        //Potential match
        var sub = str.substring(min, max + 1);
        if (word == sub) {
            return {
                match: true,
                word: word,
                index: index,
                min: min,
                max: max,
            };
        }
        else if (word == sub.split('').reverse().join('')) {
            return {
                match: true,
                word: word,
                index: index,
                min: max,
                max: min,
            };
        }
        i++;
    }

    return { match: false };
};

/*
Coverts match details into locations on diagonal forward strings. Inserts
locations into this.matches
*/
WordSearcher.prototype.processMatchesDiagonalForwards = function (matches) {
    matches.forEach((detail) => {
        if (detail.index < this.w) {
            this.matches[detail.word] = (detail.index - detail.min) + ':' + detail.min
                                        + ' ' +
                                        (detail.index - detail.max) + ':' + detail.max;
        }
        else {
            var y = detail.index - (this.w - 1);
            this.matches[detail.word] = (y + detail.min) + ':' + ((this.w - 1) - detail.min)
                                        + ' ' +
                                        (y + detail.max) + ':' + ((this.w - 1) - detail.max);
        }
    });
};

/*
Coverts match details into locations on diagonal backwards strings. Inserts
locations into this.matches
*/
WordSearcher.prototype.processMatchesDiagonalBackwards = function (matches) {
    matches.forEach((detail) => {
        if (detail.index < this.h) {
            this.matches[detail.word] = ((this.h - 1) - detail.index + detail.min) + ':' + detail.min
                                        + ' ' +
                                        ((this.h - 1) - detail.index + detail.max) + ':' + detail.max;
        }
        else {
            var x = detail.index - (this.h - 1);
            this.matches[detail.word] = detail.min + ':' + (x + detail.min)
                                        + ' ' +
                                        detail.max + ':' + (x + detail.max);
        }
    });
};

/*
Prints all the locations in this.matches
*/
WordSearcher.prototype.printMatches = function() {
    this.wordList.forEach((word) => {
        console.log(word + ' ' + this.matches[word.replace(/ /g,'')]);
    });
}

var ws = new WordSearcher();
ws.initialize();
ws.beginSearch();
ws.printMatches();
